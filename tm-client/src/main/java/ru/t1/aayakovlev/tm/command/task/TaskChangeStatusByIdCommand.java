package ru.t1.aayakovlev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.TaskChangeStatusByIdRequest;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import java.util.Arrays;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Change task status by id.";

    @NotNull
    public static final String NAME = "task-change-status-by-id";

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.print("Enter id: ");
        @NotNull final String id = nextLine();
        System.out.println("Statuses to enter:");
        System.out.println(Arrays.toString(Status.values()));
        System.out.print("Enter new status: ");
        @NotNull final String statusValue = nextLine();
        final Status status = Status.toStatus(statusValue);

        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(getToken());
        request.setId(id);
        request.setStatus(status);

        getTaskEndpoint().changeTaskStatusById(request);
    }

}
