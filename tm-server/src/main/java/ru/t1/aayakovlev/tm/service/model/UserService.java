package ru.t1.aayakovlev.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.model.User;

public interface UserService extends BaseService<User> {

    @NotNull
    User create(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception;

    @NotNull
    User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws Exception;

    @NotNull
    User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws Exception;

    @NotNull
    User findByLogin(@Nullable final String login) throws AbstractException;

    @NotNull
    User findByEmail(@Nullable final String email) throws AbstractException;

    boolean isLoginExists(@Nullable final String login) throws AbstractException;

    boolean isEmailExists(@Nullable final String email) throws AbstractException;

    @NotNull
    User lockUserByLogin(@Nullable final String login) throws AbstractException;

    void removeByLogin(@Nullable final String login) throws AbstractException;

    void removeByEmail(@Nullable final String email) throws AbstractException;

    @NotNull
    User setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) throws AbstractException;

    @NotNull
    User unlockUserByLogin(@Nullable final String login) throws AbstractException;

    @NotNull
    User update(@Nullable final User user) throws AbstractException;

    @NotNull
    User update(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String lastName
    ) throws AbstractException;

}
