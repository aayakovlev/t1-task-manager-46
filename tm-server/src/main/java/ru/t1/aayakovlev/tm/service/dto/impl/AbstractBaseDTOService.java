package ru.t1.aayakovlev.tm.service.dto.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.model.AbstractModelDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.repository.dto.BaseDTORepository;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.dto.BaseDTOService;

import javax.persistence.EntityManager;
import java.util.*;

public abstract class AbstractBaseDTOService<E extends AbstractModelDTO, R extends BaseDTORepository<E>>
        implements BaseDTOService<E> {

    @NotNull
    protected final ConnectionService connectionService;

    public AbstractBaseDTOService(@NotNull final ConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    protected abstract R getRepository(@NotNull final EntityManager entityManager);


    @NotNull
    @Override
    public E save(@Nullable final E model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        @Nullable E resultEntity;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final R entityRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            resultEntity = entityRepository.save(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        return resultEntity;
    }

    @NotNull
    @Override
    public Collection<E> add(@Nullable final Collection<E> models) throws AbstractException {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        @Nullable Collection<E> resultEntities = new ArrayList<>();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final R entityRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            for (@NotNull final E entity : models) {
                @NotNull final E resultEntity = entityRepository.save(entity);
                resultEntities.add(resultEntity);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        return resultEntities;
    }

    @Override
    public void clear() throws AbstractException {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final R entityRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            entityRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public int count() throws AbstractException {
        @NotNull final EntityManager entityManager = getEntityManager();
        int result;
        try {
            @NotNull final R entityRepository = getRepository(entityManager);
            result = entityRepository.count();
        } finally {
            entityManager.close();
        }
        return result;

    }

    @Override
    public boolean existsById(@Nullable final String id) throws AbstractException {
        try {
            findById(id);
        } catch (@NotNull final EntityNotFoundException e) {
            return false;
        }
        return true;
    }

    @NotNull
    @Override
    public List<E> findAll() throws AbstractException {
        @Nullable List<E> resultEntities;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final R entityRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            resultEntities = entityRepository.findAll();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        return resultEntities;
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final Comparator<E> comparator) throws AbstractException {
        @Nullable List<E> resultEntities;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final R entityRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            resultEntities = entityRepository.findAll(comparator);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        return resultEntities;
    }

    @NotNull
    @Override
    public E findById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable E resultEntity;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final R entityRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            resultEntity = entityRepository.findById(id);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
        if (resultEntity == null) throw new EntityNotFoundException();
        return resultEntity;
    }

    @Override
    public void remove(@Nullable final E model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        removeById(model.getId());
    }

    @Override
    public void removeById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(id)) throw new EntityNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final R entityRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            entityRepository.removeById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Collection<E> set(@Nullable final Collection<E> models) throws AbstractException {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        clear();
        return add(models);
    }

    @NotNull
    @Override
    public E update(@Nullable final E model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(model.getId())) throw new EntityNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @Nullable E resultEntity;
        try {
            @NotNull final R entityRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            resultEntity = entityRepository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        if (resultEntity == null) throw new EntityNotFoundException();
        return resultEntity;
    }

}
